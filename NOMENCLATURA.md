# Guía de estilos Sass

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | Nomenclatura | [Estructura de Archivos](ESTRUCTURA-ARCHIVOS.md) | [SassLint](SASS-LINT.md)

## Nomenclatura

En general se deben seguir las siguientes reglas:

* Las *variables*, *funciones* y *mixins* deben estar escritas en minúsculas y guiones.
* Las constantes deben estar escritas en mayusculas y con guiones.
* Los nombre de variables deben ser claras e indicar el tipo de contenido.

```sass
// Malas practicas
$rojo: #cc0;
$ColorHeaderLINK: #000;

@mixin SIZE($width, $height) {
  // ...
}
```

```sass
// Buenas practicas
$color-header: #cc0;
$color-header-link: #000;

@mixin size($width, $height) {
  // ...
}
```

### Variables

Se deben emplear variables en las siguiente ocasiones.

* Cuando un valor se repite al menos dos veces.
* Es probable que un valor pueda cambiar en el futuro.
* No declarar excesivamente variables.