# Guía de estilos Sass

Una guía de estilos es un conjunto de reglas para escribir código de forma coherente y estandarizada. Permite escribir código reutilizable y fácil de entender por todo el equipo.

### Guía de estilos

Las reglas a seguir para redactar **Sass** son:

* [Formato de código](FORMATO.md)
* [Comentarios](COMENTARIOS.md)
* [Nomenclatura](NOMENCLATURA.md)
* [Estructura de Archivos](ESTRUCTURA-ARCHIVOS.md)
* [SassLint](SASS-LINT.md)

### Referencias

* Sass Guidelines [https://sass-guidelin.es/es/](https://sass-guidelin.es/es/)
* CSS guidelines [http://cssguidelin.es/](http://cssguidelin.es/)
* Airbnb CSS / Sass Styleguide [https://github.com/airbnb/css](https://github.com/airbnb/css)
* SassLint [https://github.com/sasstools/sass-lint](https://github.com/sasstools/sass-lint)
