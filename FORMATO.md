# Guía de estilos Sass

Formato de código | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | [Estructura de Archivos](ESTRUCTURA-ARCHIVOS.md) | [SassLint](SASS-LINT.md)

## Formato de código

En general se deben seguir las siguientes reglas:

* La identación debe ser 4 espacios en blanco en lugar de tabulaciones.
* Por cada anidación de estilos se deben agregar un nivel de identación.
* Se recomienda colocar una propiedad por línea.
* Toda declaración de propiedad debe terminar en punto y coma.
* Se deben declarar primero las propiedades y luego los selectores anidados.


```sass
// mala práctica
.header {
  margin: 0;
  font-size: 36px;
  
  .link {
    color: #fff;
    text-decoration: none;
  }
  
  line-height: 36px;
    background: none;
  padding-top: 0;
}
```

```sass
// buena práctica
.header {
  margin: 0;
  font-size: 36px;
  line-height: 36px;
  background: none;
  padding-top: 0;
  
  .link {
    color: #fff;
    text-decoration: none;
  }
}
```

### Espacios en blanco

Se deben emplear espacios en blanco en las siguientes ocasiones:

* Todo selector debe estar separado del paréntesis por un espacio.
* Entre la propiedad y los dos puntos no debe haber espacio.
* Antes del valor de una propiedad debe haber un espacio en blanco.
* Para datos separados por comas, están deben tener un espacio después de la coma.

```sass
// mala práctica
.header{
  margin:0;
  font-size : 36px;
}
```

```sass
// buena práctica
.header {
  margin: 0;
  font-size: 36px;
}
```

### Líneas en blanco

Las líneas en blanco permiten separar bloques de código para facilitar la lectura. Se debe agregar una línea en blanco en las siguientes ocasiones:

* Entre la declaración de dos selectores.
* Antes de agregar selector anidado.
* Antes de un comentario.

### Cadenas de texto

* Se recomienda forzar la codificación a `utf-8` en la hoja de estilos principal.
* Las cadenas de texto deben escribirse entre comillas simples.
* Las urls siempre deben estar entre comillas.

```sass
@charset 'utf-8';

.logo {
    background: url('img/logo.png');
}
```

### Números

* Siempre se deben mostrar los ceros a la izquierda en un valor decimal.
* No se deben mostrar ceros adicionales a la derecha de un valor decimal.
* Cuando un valor es cero, no se debe agregar nombre de la unidad.

### Colores

* Se puede utilizar los nombres de colores reservados.
* Se recomienda el uso de colores escritos en RGB o RGBA.
* Si se utiliza RGB o RGBA después de la coma debe haber un espacio en blanco.
* Si se utiliza el formato hexadecimal, debe ser en minúscula y en formato corto.

```sass
// mala practica
$title-color: rgba(0,0,0,0.5);
$link-color: #CCCCCC;
```

```sass
// buena practica
$title-color: rgba(0, 0, 0, 0.5);
$link-color: #ccc;
```
