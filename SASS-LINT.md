# Guía de estilos Sass

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | [Estructura de Archivos](ESTRUCTURA-ARCHIVOS.md) | SassLint

### SassLint

[SassLint](https://github.com/sasstools/sass-lint) es una herramienta para analizar la sintaxis y asegurarse que la guía de estilos se cumple. Se puede utilizar como una tarea en Gulp o mediante un plugin para el editor de código de su preferencia.

Es recomendable verificar siempre que nuestro código pasa el test de *SassLint*.

### Instalación

SassLint se puede instalar como un módulo de NPM de la siguiente manera:

```bash
npm install -g sass-lint
```

Para instalar SassLint en un proyecto y marcarlo como requerido:

```bash
npm install sass-lint --save-dev
```

### Configuración

SassLint se puede configurar para incrementar o disminur el nivel  de validación (omitir, opcional, requerido). Para ello se requiere crear un archivo llamado `.sass-lint.yml` que se coloca en carpeta principal del proyecto.

El archivo de configuración contiene una serie de parámetros cada uno de ellos con tres opciones:

* 0: no se valida
* 1: se valida como opcional, devuelve un `warning`
* 2: se valida como requerido, devuelve un `error`

Para los proyectos de LIQUID, el archivo de configuración base se puede descargar desde: [.sass-lint.yml](.sass-lint.yml)

### Herramientas

* Brackets Sass Lint [https://github.com/petetnt/brackets-sass-lint](https://github.com/petetnt/brackets-sass-lint)
* SublimeLinter plugin for Sass/SCSS [https://github.com/skovhus/SublimeLinter-contrib-sass-lint](https://github.com/skovhus/SublimeLinter-contrib-sass-lint)
* Atom linter-sass-lint [https://atom.io/packages/linter-sass-lint](https://atom.io/packages/linter-sass-lint)