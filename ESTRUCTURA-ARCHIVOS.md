# Guía de estilos Sass

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | Estructura de Archivos | [SassLint](SASS-LINT.md)

## Estructura de Archivos

*Sass* permite separar el código en archivos diferentes con el propósito de facilitar el mantenimiento. Al momento de generar el archivos CSS se unen utilizando `@import`. Para proyectos pequeños o landings no es necesario separar el código en varios archivos pero para proyectos medianos y grandes es de gran ayuda para la organización del código.

Para facilitar el orden de los archivos se recomienda utilizar la siguiente estructura de archivos.

* `base/`
* `components/`
* `layout/`
* `pages/`
* `themes/`
* `helpers/`
* `vendors/`

### Carpeta: base

Debe contener el estilos generales que se utilizan en toda la web.

* `_reset.scss`
* `_typography.scss`
* `_base.scss`

### Carpeta: layout

Debe contener los estilos relacionados a la distribución de la web, como la grilla y las diferentes secciones que se repiten en toda la web.

* `_grid.scss`
* `_header.scss`
* `_footer.scss`
* `_sidebar.scss`
* `_forms.scss`

### Carpeta: components

Se consideran componenentes o módulos a bloques que se pueden utilizar en diferentes partes, pero son opcionales y no estan disponibles en toda la web. 

* `_slideshow.scss`
* `_carousel.scss`
* `_thumbnails.scss`
* `_calendar.scss`

### Carpeta: themes

Para proyectos grandes se tienen diferentes temas, que son variaciones de diseño pero con las mismas funcionalidades.

* `_theme.scss`
* `_admin.scss`

### Carpeta: helpers

Esta carpeta debe contener todas las herramientas para la construcción de los estilos: *mixins*, *funciones*, *variables*.

* `_variables.scss`
* `_mixins.scss`
* `_functions.scss`

### Carpeta: vendors

En esta carpeta podemos colocar todos los archivos externos y de librerías de uso general como jQuery, Bootstrap, etc.

* `_normalize.scss`
* `_bootstrap.scss`
* `_jquery-ui.scss`
* `_select2.scss`