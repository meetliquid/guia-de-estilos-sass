# Guía de estilos Sass

[Formato de código](FORMATO.md) | Comentarios | [Nomenclatura](NOMENCLATURA.md) | [Estructura de Archivos](ESTRUCTURA-ARCHIVOS.md) | [SassLint](SASS-LINT.md)

## Comentarios

Los comentarios son importantes por que permiten agregar información para el mantenimiento del código con ayuda de la lógica de creación de los estilos.

En general se deben seguir las siguientes reglas:

* Se debe comentar todo lo que no es evidente o no esta claro.
* Utilizar los comentarios en línea `//` cuando no se desea exponer el comentario en el CSS resultante.
* Utilizar los comentarios multiline `/* ... */` cuando se desea exponer el comentario en el CSS resultante.
* Se recomienda comentar los mixins y funciones.
* Se recomienda comentar la razón detrás de un número mágico.

```sass
/**
 * Clase que corta y añade puntos suspensivos para que una 
 * cadena demasiado larga quepa en una sola línea
 */
.ellipsis {
  white-space: nowrap; 
  text-overflow: ellipsis;
  overflow: hidden;
}
```

```sass
// Clase que corta y añade puntos suspensivos para que una 
// cadena demasiado larga quepa en una sola línea
.ellipsis {
  white-space: nowrap; 
  text-overflow: ellipsis;
  overflow: hidden;
}
```

### Comentarios para Documentación

En el caso de Sass se puede documentar utilizando el plugin SassDoc que convierte los comentarios en documentación.

* Para documentar se deben utilizar tres barras diagonales.
* Antes de cada comentarios de documentación incluir una línea en blanco.

```sass
/// Color de fondo del header
/// @type Color
$color-header: #cc0;

/// Color de los enlaces del header
/// @type Color
$color-header-link: #000;
```
